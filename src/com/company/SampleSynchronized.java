package com.company;

public class SampleSynchronized {

    public synchronized void testMethodA(){
        System.out.println("Test Method A");
        try {
            Thread.sleep(60000l);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public synchronized void testMethodB(){
        System.out.println("Test Method B");
        try {
            Thread.sleep(60000l);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void testMethodC(){
        System.out.println("Test Method C");
    }
}
