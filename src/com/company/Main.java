package com.company;

import com.company.model.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public class Main {

    private Comergent cmgt;

    public enum Conversion {
        Y("Success"),
        E("ERROR"),
        N("InProgress");

        String value;

        Conversion(String value){
            this.value = value;
        }

        public String getValue(){
            return value;
        }
    }

    public static void main(String[] args) {
        //dateTest();

        //formatToString();

        //streamTest();

        //timeStampManip();

       /* try {
            new Main().testObjectToXml();
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        //checkStatus();
        //parallelExecution();
        //normalExecution();
        //testLongToTimestamp();
        //System.out.println(Conversion.valueOf("K").getValue());
        //threadSyncExample();
        //collectMapNullValue();
        //csvFileReplace();
        Long l = null;
        String s = String.valueOf(l);
        System.out.println(s);
    }

    private static void csvFileReplace() {
        try {
            String str = new String(Files.readAllBytes(Paths.get("Hello.csv")));
            //str.replaceAll("");
            System.out.println(str.replace("\"",""));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void collectMapNullValue() {
        List<LineItems> line = new ArrayList<>();
        line.add(new LineItems(1l));
        line.add(new LineItems(2l));
        line.add(new LineItems(3l,"Hello"));
        Map<Long,String> lines = line.stream().filter(s-> s.getDlmId()!=null).collect(Collectors.toMap(LineItems::getLineKey,LineItems::getDlmId));
        System.out.println("ABC"+lines.get(3l));
    }

    private static void threadSyncExample() {
        SampleSynchronized s1 = new SampleSynchronized();
        SampleSynchronized s2 = new SampleSynchronized();
        Thread t1 = new Thread(()->s1.testMethodA());
        t1.start();
        Thread t2 = new Thread(()->s2.testMethodA());
        t2.start();
    }

    private static void normalExecution() {
        System.out.println(System.currentTimeMillis());
        String skus = "BB134,BB345,BB567,1234,bb5566,bb678,8990,bb7890,bb8900,bb9000";
        String[] skuList = skus.split(",");
        boolean canAdd = false;
        for (String s : skuList) {
            canAdd = canAdd && isSKUValid(s);
        }
        System.out.println(System.currentTimeMillis());
    }

    private static void parallelExecution() {
        System.out.println(System.currentTimeMillis());
        String skus = "BB134,BB345,BB567,1234,bb5566,bb678,8990,bb7890,bb8900,bb9000";
        String[] skuList = skus.split(",");
        List<Callable<Boolean>> callables = new ArrayList<>();
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        for (String s : skuList) {
            Callable<Boolean> task = () ->{
                return isSKUValid(s);
            };
            callables.add(task);
        }
        try {
            List<Future<Boolean>> futureList = executorService.invokeAll(callables);
            AtomicBoolean canAdd = new AtomicBoolean(false);
            futureList.forEach(future -> {
                try {
                    canAdd.set(canAdd.get() && future.get());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            });
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        executorService.shutdown();
        executorService.shutdownNow();
        System.out.println(System.currentTimeMillis());
    }

    public static Boolean  isSKUValid(String sku){
        System.out.println(sku);
        return sku!=null && sku.toUpperCase().startsWith("BB");
    }

    private static void checkStatus() {
        String success = "SUCCESS";
        //listTest1();
        //testLongToTimestamp();
        List<String> s = new ArrayList<>();
        s.add("SUCCESS");
        s.add("PENDINGU");

        List<String> collect = s.stream().filter(p -> p != success).collect(Collectors.toList());
        collect.forEach(System.out::println);
    }

    private static void testLongToTimestamp() {
        long l = 1559251426788l;
        Timestamp t = new Timestamp(l);
        System.out.println(t);
    }

    private static void listTest1() {
        List<Long> list1 = new ArrayList<>();
        list1.add(1l);
        list1.add(2l);
        list1.add(3l);

        List<Long> list2 = new ArrayList<>();
        list2.add(1l);
        list2.add(4l);

        List<Long> list3 = new ArrayList<>();
        list3.addAll(list1);
        list3.removeAll(list2);

        System.out.println(list1);
        System.out.println(list3.toString());
    }

    private static void timeStampManip() {
        Timestamp t = Timestamp.from(Instant.now());
        Timestamp t1 = new Timestamp(new Date().getTime());
        System.out.println("t: "+t);
        System.out.println("t1: "+t1);
    }

    private static void streamTest() {
        List<Long> list = new ArrayList<>();
        list.add(1l);
        list.add(2l);
        list.add(1l);
        System.out.println("Coll size:"+list.size());

        System.out.println(list.stream().collect(Collectors.toSet()).size());
    }

    private static void formatToString() {
        Long l = null;

        System.out.println(Optional.ofNullable(l).map(Object::toString).orElse(""));
        System.out.println(Objects.toString(l,""));
    }

    private static void dateTest() {
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        System.out.println(sdf.format(d));
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        System.out.println(sdf.format(d));
    }

    public void setUp(){
        Transaction t1 = new Transaction();
        LineItem l2 = new LineItem(2l,null);
        //l2.setDescription(null);
       // l2.setLineKey(2l);
        t1.add(new LineItem(1l,""));
        t1.add(l2);

        Transaction t2 = new Transaction();
        t2.add(new LineItem(3l,"Hello"));

        cmgt = new Comergent(new Header());
        cmgt.add(t1);
        cmgt.add(t2);
    }

    public void testObjectToXml() throws JAXBException, IOException {

        setUp();
        JAXBContext jaxbContext = JAXBContext.newInstance(Comergent.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty("com.sun.xml.bind.xmlHeaders",
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        marshaller.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
        marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,true);

        StringWriter str = new StringWriter();
        marshaller.marshal(cmgt, new File("linesnew.xml"));
        marshaller.marshal(cmgt, str);
        String s = str.toString();
        FileWriter f = new FileWriter("linesnew1.xml");
        f.write(s);
        f.close();

        //IndentingXMLStreamWriter xmlStreamWriter = new IndentingXMLStreamWriter();
       // marshaller.marshal(cmgt, new XML);


        marshaller.marshal(cmgt, System.out);
    }
}